/** 
 * GeneticSolver.js
 *
 * Description: 
 *     A genetic solver a 2d run generator.
 * 
 * Author: John Dunham
 * Date Modified: 3/26/13
 */
function GeneticSolver (genome,randomFunc, mutationFunc, updateStat){
    this.newGene = randomFunc;
    this.mutate  = mutationFunc;
    this.updateStat = updateStat;
    this.genome = genome;
    this.mutationRate = .75;
    this.generationSize = 0;
    this.maxGenerations = 0;
    this.savePerGeneration = 1;
    
    this.activeGenes = [];
    this.populationGenes = [];

    this.generationRecord   = [];
	this.outstandingGenes = [];
    this.max                = null;
    this.min                = null;
    this.currentGeneration  = 0;
    this.currentGene        = 0;    
}

GeneticSolver.prototype = {
    initPop:function(isNew){		
		if(isNew){
			this.activeGenes = [];

			for(var gene = 0; gene < this.generationSize; gene++){
				this.activeGenes.push({"gene":this.newGene(),"fitness":0,"fNorm":0});
			}
		}
			
        this.currentGeneration  = 0;
        this.generationRecord   = [];
		this.outstandingGenes = []
        this.currentGene        = 0;   
        this.max                = null;
        this.min                = null;
        
    },
	initForOutstanding:function(){
		this.currentGene    = 0;
		this.generationSize = this.outstandingGenes.length;
	},
	
    next:function(){
        if(this.currentGeneration >= this.maxGenerations){
            return null;            
        }
        else if(this.currentGene < this.generationSize){	
            return this.activeGenes[this.currentGene++];
        }
        else{
            this.nextGeneration();
            this.updateStat();
            return this.next();
        }
    },
	
	nextOutstanding:function(){
		if(this.currentGene < this.generationSize){	
            return this.outstandingGenes[this.currentGene++];
        }
		else 
			return null;
	},
    
    nextGeneration:function(){    
        this.currentGeneration++;
        this.currentGene = 0;
        var adjustedFitnessSum  = 0, actualFitnessSum = 0;
        var roulette    = 0;
        var parentA,parentB;
        var parentIndexA = 0;
        var localMin  = 0;
        
        // Presort the active gene population.
        this.activeGenes.sort(this.compare);
        
        
        localMin = this.activeGenes[this.activeGenes.length - 1].fitness -1;
        
        // Determine normalized fitnesses.
        for(var i = 0; i < this.activeGenes.length; i++){
            adjustedFitnessSum += this.activeGenes[i].fitness - localMin;
            actualFitnessSum +=this.activeGenes[i].fitness;
        }        
        for(var i = 0; i < this.activeGenes.length; i++){
            this.activeGenes[i].fNorm =  (this.activeGenes[i].fitness - localMin)/adjustedFitnessSum;
        }
        
        // Save a record of this generation.
        this.generationRecord.push({"max":this.activeGenes[0].fitness, 
                                    "avg":Math.floor(actualFitnessSum/this.activeGenes.length), 
                                    "min":this.activeGenes[this.activeGenes.length-1].fitness});

        if(!this.max || this.max < this.activeGenes[0].fitness){
			this.outstandingGenes.push({"gene":this.activeGenes[0].gene.slice(0), "fitness":this.activeGenes[0].fitness, "fNorm":this.activeGenes[0].fNorm, "gen":this.currentGeneration-1});
            this.max = this.activeGenes[0].fitness;
		}
        
        if(!this.min || this.min > this.activeGenes[this.activeGenes.length-1].fitness)
            this.min = this.activeGenes[this.activeGenes.length-1].fitness;

        // Reset actives and back up population. Save the top two individuals.
        for(var gene = 0; gene < this.activeGenes.length; gene++){
            this.populationGenes[gene] = null;
            this.populationGenes[gene] = this.activeGenes[gene];
            
            if(gene >= this.savePerGeneration)
                this.activeGenes[gene]    = {"gene":[],"fitness":0,"fNorm":0};
        }

        // Crossover Time!
        for(var gene = this.savePerGeneration; gene < this.activeGenes.length; gene++){
            parentA = parentB = null;
            roulette   = Math.random();
            adjustedFitnessSum = 0;
            
            for(var i = 0; i < this.activeGenes.length; i++){
                adjustedFitnessSum += this.populationGenes[i].fNorm;
                if(adjustedFitnessSum > roulette){
                    parentIndexA = i;
                    parentA = this.populationGenes[i].gene;
                    break;
                }
            }
            
            if(parentA == null){
                parentIndexA = Math.floor(Math.random() * this.activeGenes.length);
                parentA = this.populationGenes[parentIndexA].gene;
            }
            
            roulette           = Math.random();
            adjustedFitnessSum = 0;
            
            for(var i = 0; i < this.activeGenes.length; i++){
                adjustedFitnessSum += this.populationGenes[i].fNorm;
                if(adjustedFitnessSum > roulette){
                    if(parentIndexA == i){
                        i = ((i + 1) % this.activeGenes.length); 
                    }
                    parentB = this.populationGenes[i].gene;
                    break;
                }
            }
            
            if(parentB == null){
                parentB = this.populationGenes[(parentIndexA + 1) % this.activeGenes.length].gene;
            }
            
            
            if(gene + 1 == this.activeGenes.length){
                this.crossover(parentA, parentB, this.activeGenes[gene].gene,[]);
            }
            else{
                this.crossover(parentA, parentB, this.activeGenes[gene].gene,this.activeGenes[gene+1].gene);
                gene++;
            }     
        }    
    },
    
    compare:function(a, b){
        return b.fitness - a.fitness;
    },
    
    crossover:function(parentGeneA, parentGeneB, childGeneA, childGeneB){
        var pivotMax = Math.floor(Math.random() * this.genome.length);
        var pivotMin = Math.floor(Math.random() * this.genome.length);

		// If the points are equal add a random amount to the max.
		 if ( pivotMax == pivotMin)
			pivotMax += Math.floor(Math.random() * this.genome.length) % this.genome.length;

		// Swap if the pivot points are reversed
		if(pivotMax < pivotMin){
			var swap = pivotMax
			pivotMax = pivotMin;
			pivotMin = pivotMax;
		}
		
        for(var index = 0; index < this.genome.length; index++){

			// Replace parentA's genome with parentB's over interval (pivotMin,pivotMax]
            if(index > pivotMin && index <= pivotMax){
                childGeneA.push(parentGeneB[index]);
                childGeneB.push(parentGeneA[index]);
            }
            else {
				childGeneA.push(parentGeneA[index]);
                childGeneB.push(parentGeneB[index]);                
            }            
        }
        
		// Mutates based on the user specified mutation rate.
		this.mutate(childGeneA,this.mutationRate);
		this.mutate(childGeneB,this.mutationRate);
    },
	
	exportOutstanding:function(exportObj){
		exportObj.solver = {};
		
		exportObj.solver.outstanding = this.outstandingGenes;
		exportObj.solver.population  = this.populationGenes;
	},
	
	importOutstanding:function(newOutstanding){
		this.initPop();
		
		if(newOutstanding){
			this.outstandingGenes = newOutstanding.outstanding.slice(0);
			this.populationGenes  = newOutstanding.population.slice(0);
		}
	}	
}
 