// The request animation frame function, for the draw function.
window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       || 
          window.webkitRequestAnimationFrame || 
          window.mozRequestAnimationFrame    || 
          window.oRequestAnimationFrame      || 
          window.msRequestAnimationFrame     || 
          function(/* function */ callback, /* DOMElement */ element){
            window.setTimeout(callback, 33);
          };
})();

var WorldManager = (function(){
    var manager = {};

	if(window.Prototype) {
		delete Object.prototype.toJSON;
		delete Array.prototype.toJSON;
		delete Hash.prototype.toJSON;
		delete String.prototype.toJSON;
	}

    
    // box2d settings.
    manager.STEP_TIME = 60;
    manager.TIME_STEP = 1.0/60;
	manager.ITERATION = 1;
    manager.b2World;
    
    // Skeleton
    manager.baseSkeleton = new Skeleton();
    manager.headContact;
    
    // States
    manager.constructing = true;
    manager.drawing      = false;
    manager.looping      = false;
	manager.advance      = null;
    
    // Drawing helpers.
    manager.lineStart = {'x':0,'y':0};
    manager.lineEnd   = {'x':0,'y':0};
    manager.offset    = {'x':0,'y':0,'w':0,'h':0};
    manager.b2Offset  = {'x':0,'y':0,'w':0,'h':0};  
    manager.hoveredBone  = -1;
    
    // Fitness stuff.
    manager.stayAliveDistance  = 10;                     // The skeleton MUST move this far 
    manager.stayAliveTime      = manager.STEP_TIME * 5;  // The time out for stay alive checks.
    manager.stayAliveOrigin    = 0;                      // The stay alive location.
    manager.stayAliveOriginT   = 0;                      // The stay alive start time.
    manager.maxAllowedTime     = manager.STEP_TIME * 60; // The maximum amount of time a skeleton is allotted to make it to the goal.
                                                         // Please note this is time in the physics simulation, NOT REAL WORLD TIME.
    manager.winOffset          = 1100;                   // How far the skeleton head must travel for a win.
    manager.winLine            = manager.winOffset;      // The win line for the skeleton.
    manager.failLine           = -100;                   // If the head passes this it auto fails.    
    manager.timeElapsed        = 0;
	manager.maxDistance        = 0;
    manager.headDidCollide     = false;    

    // Fitness Calculations.
    manager.UNIT_VALUE         = 100000 / manager.winOffset; // The fitness of a unit in box2d space. calibrated to be 100,000 for a victory condition.
    manager.HEAD_PENALTY       = -5000;                      // A skeleton hitting its head carries a penalty equivalent to half a victory condition.
                                                             // Additionally triggers on a head collide with the ceiling.
    manager.TIME_PENALTY       = -.01;                        // Each time step adds a negative penalty to the fitness.
	manager.REVERSE_PENALTY    = .25					     // Applies a penalty to genomes that back track.

    
    // Genetic stuff.
    manager.solver;  
    manager.currentGene;    
    
    // HTML5 stuff.
    manager.ctx;
    manager.b2ctx;
    manager.canvas;
    manager.gctx;
    manager.gRect;
    
    // HTML fields.
    manager.popPer;             
    manager.generations;
    manager.mutation;
    manager.startButtons = [];
    manager.stopButton;
    manager.csv;
    manager.generation;
    manager.actives;
    manager.previous;
	
    
    manager.init = function(canvasID,b2CanvasID){
        // Create the drawer for the skeleton assembly.
        manager.canvas  = document.getElementById(canvasID);
        //var gcanvas = document.getElementById("graphCanvas");
        var b2canvas  = document.getElementById(b2CanvasID);

        var rect         = manager.canvas.getBoundingClientRect();
        manager.offset   = {'x':rect.left,'y':rect.top,'w':rect.width, 'h':rect.height};
        rect             = b2canvas.getBoundingClientRect();
        manager.b2Offset = {'x':rect.left,'y':rect.top,'w':rect.width, 'h':rect.height};
       // rect             = gcanvas.getBoundingClientRect();
       // manager.gRect    ={'x':rect.left,'y':rect.top,'w':rect.width, 'h':rect.height};

        manager.b2ctx = b2canvas.getContext('2d');
        manager.ctx   = manager.canvas.getContext('2d');
        //manager.gctx  = gcanvas.getContext('2d');

        manager.canvas.addEventListener("mousedown", this.mouseDown);
        manager.canvas.addEventListener("mouseup", this.mouseUp);
        manager.canvas.addEventListener("mousemove", this.mouseMove);
        
        // HTML elements
        document.getElementById("test").addEventListener("click", manager.startLoop, false);
        manager.startButtons.push(document.getElementById("test"));
        
        document.getElementById("start").addEventListener("click", manager.startLoop, false);
        manager.startButtons.push(document.getElementById("start"));
		
		document.getElementById("startFromPop").addEventListener("click", manager.startLoop, false);
        manager.startButtons.push(document.getElementById("startFromPop"));
		
		document.getElementById("outstanding").addEventListener("click", manager.startLoop, false);
        manager.startButtons.push(document.getElementById("outstanding"));
		
        
        document.getElementById("reset").addEventListener("click", manager.reset, false);
        manager.startButtons.push(document.getElementById("reset"));
        
        document.getElementById("stop").addEventListener("click", manager.stopLoop, false);
        manager.stopButton = document.getElementById("stop"); 
        manager.stopButton.disabled = true;
		
		document.getElementById("save").addEventListener("click", manager.exportIndividual, false);
        document.getElementById("load").addEventListener("click", manager.importIndividual, false);

                
        manager.actives  = document.getElementById("fitActive");
        manager.previous = document.getElementById("fitPrevious");
        manager.popPer = document.getElementById("population");
        manager.generations =  document.getElementById("generations");
        manager.mutation = document.getElementById("mutation");
        manager.csv = document.getElementById("stats");
        manager.generation = document.getElementById("generation");
		manager.exportField = document.getElementById("exportField");
        
        // Create the b2World.
        var worldAABB = new b2AABB();
        worldAABB.minVertex.Set(-1000, -1000);
        worldAABB.maxVertex.Set(2* manager.b2Offset.w, 1000);
        var gravity = new b2Vec2(0, 300);
        var doSleep = false;

        manager.b2World = new b2World(worldAABB, gravity, doSleep);

        
        // Creates the ground.
        var groundSd = new b2BoxDef();
        groundSd.extents.Set(manager.b2Offset.w *2, 50);
        groundSd.restitution = 0.2;
		groundSd.categoryBits = 0x0001;
		
        var groundBd = new b2BodyDef();
        groundBd.AddShape(groundSd);
        groundBd.position.Set(-1000, 340);
        groundBd.userData = {"kill":true};

        manager.b2World.CreateBody(groundBd);
        
        // Creates the ceiling.
		/*
        var ceilSd = new b2BoxDef();
        ceilSd.extents.Set(manager.b2Offset.w *2, 50);
        ceilSd.restitution = 0.2;

        var ceilBd = new b2BodyDef();
        ceilBd.AddShape(ceilSd);
        ceilBd.position.Set(-500, -100);
        ceilBd.userData = {"kill":true};
        manager.b2World.CreateBody(ceilBd);
        */
        manager.draw();
        
        // Set up the genetic solver.
        manager.solver = new GeneticSolver(Skeleton.Genome,Skeleton.CreateRandomGene,Skeleton.RandomizeGene,manager.drawGraphs);
        
    };
    
    // Skeleton Draw Code.
    // ---------------------------------
    manager.mouseDown = function(e){
        if(!manager.constructing)
            return;
            
        if(manager.drawing){
            manager.mouseUp(e);
        }

        if(manager.baseSkeleton.boneCount == 0 || manager.hoveredBone !=-1){
            manager.drawing = true;

            manager.lineStart.x = manager.lineEnd.x = e.pageX - manager.offset.x;
            manager.lineStart.y = manager.lineEnd.y = e.pageY -  manager.offset.y;
        }

    }
    
    manager.mouseMove = function(e){
        if(!manager.constructing){
            return;
        }
        
        manager.draw();
        manager.lineEnd.x   = e.pageX -  manager.offset.x;
        manager.lineEnd.y   = e.pageY -  manager.offset.y;
        
        if(!manager.drawing){
            manager.hoveredBone = manager.findBone(manager.lineEnd);
            manager.canvas.style.cursor   = manager.hoveredBone == -1 ? "default" : "pointer";        
            return;
        }
               
        manager.ctx.clearRect(0, 0, manager.offset.w, manager.offset.h);
        
        manager.ctx.beginPath();
        manager.ctx.moveTo(manager.lineStart.x, manager.lineStart.y);
        manager.ctx.lineTo(manager.lineEnd.x, manager.lineEnd.y);
        manager.ctx.stroke();
        manager.ctx.closePath();
    }
    
    manager.mouseUp = function(e){
        if(!manager.constructing || !manager.drawing)
            return;
        
        manager.baseSkeleton.addBone(manager.b2World,manager.lineStart,manager.lineEnd, manager.hoveredBone);
        manager.drawing = false;
        manager.ctx.clearRect(0, 0, manager.offset.w, manager.offset.h);
        manager.hoveredBone = manager.findBone(manager.lineEnd);
        
        if(manager.baseSkeleton.boneCount == 1){
            manager.winLine = manager.winOffset + manager.baseSkeleton.head.body.m_position.x;
        }       
    }    

    manager.findBone = function(mouse){
        var mousePVec = new b2Vec2(mouse.x, mouse.y);

        var aabb = new b2AABB();
        aabb.minVertex.Set(mouse.x - 0.001, mouse.y - 0.001);
        aabb.maxVertex.Set(mouse.x + 0.001, mouse.y + 0.001);

        // Query the world for overlapping shapes.
        var selectedBody = {};
        manager.b2World.Query(aabb,selectedBody,1);
        if(selectedBody[0] && selectedBody[0].m_body.m_userData){
            selectedBody = selectedBody[0].m_body.m_userData.bone;
            if(typeof selectedBody === "undefined")
                selectedBody = -1;            
        }
        else
            selectedBody = -1;   
            
        return selectedBody;
    }
    // --------------------------------
    
    manager.startLoop = function(e){
        if(!manager.looping && manager.baseSkeleton.boneCount !=0){
            for(var i =0; i < manager.startButtons.length; i++){
                manager.startButtons[i].disabled = true;
            }
            
            manager.stopButton.disabled = false;
        
            manager.ctx.clearRect(0, 0, manager.offset.w, manager.offset.h);
            manager.looping = true;
            manager.constructing = false;
            
            if(e.toElement.id == "start" || e.toElement.id == "startFromPop"){
                manager.update = manager.updateGenetic;

                // Set up the population for processing.
                manager.solver.generationSize = parseInt(manager.popPer.value,10);
                manager.solver.maxGenerations = parseInt(manager.generations.value,10);
                manager.solver.mutationRate   = parseFloat(manager.mutation.value);
                manager.headDidCollide        = false;
                
                manager.solver.initPop(e.toElement.id == "start");
                
				manager.advance = manager.geneticsAdvance;
				manager.advance();               
            }            
            else if(e.toElement.id == "test"){
                manager.update = manager.updateTest;
            }
			else if(e.toElement.id == "outstanding"){
				manager.update = manager.updateGenetic;
				manager.advance = manager.outstandingAdvance;
				
				manager.solver.initForOutstanding();
				manager.advance();
			}
			
			manager.csv.value = "";
			manager.actives.innerHTML = "";
			manager.previous.innerHTML = "";
			
            manager.loop();
        }
    }
	
	manager.fitness = function(){
		// Apply points
		manager.currentGene.fitness = 
			(manager.maxDistance - (manager.winLine - manager.winOffset)) * manager.UNIT_VALUE;
		// Apply penalties
		manager.currentGene.fitness += (manager.baseSkeleton.head.body.m_position.x - manager.maxDistance) * manager.REVERSE_PENALTY;
		manager.currentGene.fitness += (manager.headDidCollide ? manager.HEAD_PENALTY : 0) + manager.timeElapsed * manager.TIME_PENALTY;
		manager.currentGene.fitness = Math.floor(manager.currentGene.fitness);
		manager.actives.innerHTML += manager.currentGene.fitness + "<br>";
	}
	
	manager.outstandingAdvance = function(){
		if(manager.currentGene){
          manager.fitness();
        }	
		
		manager.currentGene = manager.solver.nextOutstanding();	
       
		
		if(manager.solver.currentGene == 1){
            manager.previous.innerHTML = manager.actives.innerHTML;
            manager.actives.innerHTML = "";
        }
		
		if(!manager.currentGene){
            manager.stopLoop();
            return;
        }
		
        manager.generation.innerHTML= "Current Generation: " + manager.currentGene.gen + 
                                      "<br>Current Individual: " + manager.solver.currentGene;        
        manager.baseSkeleton.applyGene(manager.currentGene);
        manager.baseSkeleton.reset(manager.b2World);
        
        // Reset anything that keeps track of anything.
        manager.timeElapsed      = 0;     
        manager.headDidCollide   = false;    
        manager.stayAliveOrigin  = manager.baseSkeleton.head.body.m_position.x;
        manager.stayAliveOriginT = 0;
		manager.maxDistance      = 0
	}
    
	
    // Interrupts the draw loop, handles fitness and gene advancement.
    manager.geneticsAdvance = function(){
        // Find the fitness.
        if(manager.currentGene){
          manager.fitness();
        }
		
        // Get the next gene
        manager.currentGene = manager.solver.next();
        manager.generation.innerHTML= "Current Generation: " + manager.solver.currentGeneration + 
                                      "<br>Current Individual: " + manager.solver.currentGene;
                                      
        if(manager.solver.currentGene == 1){
            manager.previous.innerHTML = manager.actives.innerHTML;
            manager.actives.innerHTML = "";
        }                          
                          
        // The dream is over.
        if(!manager.currentGene){
            manager.stopLoop();
            return;
        }
        
        manager.baseSkeleton.applyGene(manager.currentGene);
        manager.baseSkeleton.reset(manager.b2World);
        
        // Reset anything that keeps track of anything.
        manager.timeElapsed      = 0;     
        manager.headDidCollide   = false;    
        manager.stayAliveOrigin  = manager.baseSkeleton.head.body.m_position.x;
        manager.stayAliveOriginT = 0;
		manager.maxDistance      = 0
    }
    
    
    // Stop Simulations
    manager.stopLoop = function(){
        if(manager.looping){
            manager.looping = false;
            manager.constructing = true;
            manager.timeElapsed = 0;      
            
            manager.baseSkeleton.reset(manager.b2World);
            manager.draw();

            for(var i =0; i < manager.startButtons.length; i++){
                manager.startButtons[i].disabled = false;
            }
            
            manager.b2World.Step(manager.TIME_STEP, manager.ITERATION);

            manager.stopButton.disabled = true;            
        }
    }    
    
    // "Game Loop" functionality.
    // ------------------------------
    manager.draw = function(){
        manager.b2ctx.clearRect(0, 0, manager.b2Offset.w, manager.b2Offset.h);
        drawWorld(manager.b2World, manager.b2ctx, manager.winLine);
    }
    
    manager.updateTest = function(){
		
        manager.b2World.Step(manager.TIME_STEP, manager.ITERATION);	
    }
	
	manager.updateOutstanding = function(){
		
        manager.b2World.Step(manager.TIME_STEP, manager.ITERATION);	
    }
    
    manager.updateGenetic = function(){
		var actualJoint = {};
		manager.baseSkeleton.joints.forEach(function(joint){
			if(joint.motor.isActive){
				actualJoint = manager.baseSkeleton.bones[joint.body.bone].joints[joint.body.joint];
				// Reverse if at limit or if the joint isn't moving anymore.
				if(actualJoint.GetJointAngle() > (actualJoint.m_upperAngle + actualJoint.m_intialAngle - joint.motor.limit.up) ||
						actualJoint.GetJointAngle() < (actualJoint.m_lowerAngle + actualJoint.m_intialAngle - joint.motor.limit.low)){
					joint.motor.flip *=-1;
				}
				else if(joint.motor.prevAngle == actualJoint.GetJointAngle()) { // The joint is stuck before a limit.
					if(++joint.motor.stuckCount >= joint.motor.stuckTime) {
						joint.motor.stuckCount = 0;
						joint.motor.flip *=-1;
					}
				}
				else{
					joint.motor.stuckCount = 0;
				}
				
				actualJoint.SetMotorSpeed(joint.motor.flip * joint.motor.speed);
				joint.motor.isActive = ++joint.motor.aV > joint.motor.active;

				if(!joint.motor.isActive)
					joint.motor.aV = 0;
				
			}
			else{				
				joint.motor.isActive = ++joint.motor.rV > joint.motor.rest;
				if(joint.motor.isActive)
					joint.motor.rV = 0;
			}
			
			joint.motor.prevAngle = manager.baseSkeleton.bones[joint.body.bone].joints[joint.body.joint].GetJointAngle();
		});
		        
        manager.b2World.Step(manager.TIME_STEP, manager.ITERATION);
        manager.timeElapsed++;        

        // Check to see that the skeleton is moving enough.
        if(manager.baseSkeleton.head.body.m_position.x > (manager.stayAliveOrigin + manager.stayAliveDistance) ){
            manager.stayAliveOriginT = manager.timeElapsed;
            manager.stayAliveOrigin = manager.baseSkeleton.head.body.m_position.x ;
        }
		
		if(manager.baseSkeleton.head.body.m_position.x  > manager.maxDistance){
			manager.maxDistance = manager.baseSkeleton.head.body.m_position.x;
		}

        // Check to see if the head has collided with a "kill object"
        manager.headContact = manager.baseSkeleton.head.body.GetContactList();
        
        while(manager.headContact && !manager.headDidCollide){
            if(manager.headContact.other.m_userData)
                manager.headDidCollide = manager.headContact.other.m_userData.kill ? true : manager.headDidCollide;
            manager.headContact = manager.headContact.next;
        }
            
        // If a loss or win condition is detected time to move on to a new 
        if(manager.baseSkeleton.head.body.m_position.x > manager.winLine    || 
            manager.headDidCollide                                          || 
            manager.baseSkeleton.head.body.m_position.x  < manager.failLine ||
            manager.timeElapsed > manager.maxAllowedTime                    ||
            manager.timeElapsed > (manager.stayAliveOriginT + manager.stayAliveTime))
                manager.advance();
        }
    
    manager.loop = function(){
        manager.update();
        manager.draw();

        if(manager.looping)
            window.requestAnimFrame(manager.loop);
    }
    // ------------------------------
    
    // Reporting functionality
    manager.drawGraphs = function(){
		/*
		var widthPerStep  = manager.gRect.w/(manager.solver.generationRecord.length);
        var heightPerStep = (manager.gRect.h)/(manager.solver.max);
		*/
        var csv = "";
        /*
		manager.gctx.clearRect(0, 0, manager.gRect.w, manager.gRect.h);        
        manager.drawTrend('#00FF00',widthPerStep,heightPerStep,"max");
        manager.drawTrend('#0000FF',widthPerStep,heightPerStep,"avg");
        manager.drawTrend('#FF0000',widthPerStep,heightPerStep,"min");  
		*/
        for(var i = 0; i < manager.solver.generationRecord.length; i++){
            csv += manager.solver.generationRecord[i].max + ","+manager.solver.generationRecord[i].avg + "," + manager.solver.generationRecord[i].min + "\n";
        }
        
        manager.csv.value = csv;
    }
    
    manager.drawTrend = function (stroke, wPer, hPer, value){
        manager.gctx.beginPath();
        manager.gctx.strokeStyle = stroke;
        manager.gctx.moveTo(0,manager.gRect.h - manager.solver.generationRecord[0][value] * hPer);

        for(var i = 0; i < manager.solver.generationRecord.length; i++){
		console.log(i*wPer);
            manager.gctx.lineTo(i*wPer,manager.solver.generationRecord[i][value] * hPer);
        }
        
        manager.gctx.stroke();
        manager.gctx.closePath();
    }    
    // ------------------------------
    
    // QOL code
    manager.reset = function(){
        if(confirm("Do you wish to delete your skeleton?")){
            manager.baseSkeleton.destroy(manager.b2World);
            manager.draw();
        }
    }
    
    manager.exportIndividual = function(){
		var exportObj = {}
        manager.baseSkeleton.exportSkeleton(exportObj);
		manager.solver.exportOutstanding(exportObj);
		manager.exportField.value = JSON.stringify(exportObj);
    }
	
	manager.importIndividual = function(){
		var importObj = JSON.parse(manager.exportField.value);
		
		if(importObj.skeleton && manager.baseSkeleton.importSkeleton(manager.b2World,importObj.skeleton)){
		
			manager.solver.importOutstanding(importObj.solver);		            
		}		
		
		if(manager.baseSkeleton.head.body)
			manager.winLine = manager.winOffset + manager.baseSkeleton.head.body.m_position.x;
		
		manager.draw();
    }
    
    return manager;
}()) || {};
