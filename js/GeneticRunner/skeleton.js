function Skeleton(headCallbackFunc){
    this.bones = [];
    this.joints = []; // for genome assignment.
    this.head  = {};
    this.headCallback = headCallbackFunc;
    this.boneCount =0;
    
    Skeleton.Genome = [];
}

// Imposes a maximum number of bones to reduce genome size.
Skeleton.HeadRadius = 20;
Skeleton.BoneLimit = 20;
Skeleton.Width = 3;
Skeleton.Density = .000001;
Skeleton.HeadMask = 0x0002;
Skeleton.BoneMask = 0x0004;
Skeleton.MaskBase = 0xFFFF;
Skeleton.MaxTorque = 100;
Skeleton.Genome = [];
Skeleton.GeneMaxima = [3,-3,6,30,30,30,false]; // Angle Max, Angle Min, Speed, Active Time, Rest Time, Stuck Time, Active Start

Skeleton.AppendJoint = function(){
    for (var i = 0; i < Skeleton.GeneMaxima.length; i++){
        Skeleton.Genome.push(0);  
    }
}

Skeleton.CreateRandomGene = function(gene){
    gene = gene || [];
    
	// Determine the Range
	for(var i =0; i < Skeleton.Genome.length; i += Skeleton.GeneMaxima.length){
		// Max
		gene[i] = Math.random() *  (Skeleton.GeneMaxima[0] + 1);
		// Min
		gene[i+1] = Math.random() *  (Skeleton.GeneMaxima[1] + 1);
		// Speed
		gene[i+2] = 2 * Math.random() *  (Skeleton.GeneMaxima[2] + 1) - Skeleton.GeneMaxima[2];
		// Active time.
		gene[i+3] = Math.floor(Math.random() *  (Skeleton.GeneMaxima[3] + 1)) + 1;
		// Rest time.
		gene[i+4] = Math.floor(Math.random() *  (Skeleton.GeneMaxima[4] + 1)) + 1;
		// Stuck time.
		gene[i+5] = Math.floor(Math.random() *  (Skeleton.GeneMaxima[4] + 1)) + 1;
		// Active at start.
		gene[i+6] = Math.random() < .5;
	}
	
    return gene;
}

Skeleton.RandomizeGene = function(gene, mutationRate){
	for(var i =0; i < gene.length; i += Skeleton.GeneMaxima.length){
		// Max
		if(Math.random() < mutationRate)
			gene[i] = Math.random() *  (Skeleton.GeneMaxima[0] + 1); 
		// Min
		if(Math.random() < mutationRate)
			gene[i+1] = Math.random() *  (Skeleton.GeneMaxima[1] + 1); 				
		// Speed
		if(Math.random() < mutationRate)
			gene[i+2] = 2 * Math.random() *  (Skeleton.GeneMaxima[2] + 1) - Skeleton.GeneMaxima[2];
		// Active time.
		if(Math.random() < mutationRate)
			gene[i+3] = Math.floor(Math.random() *  (Skeleton.GeneMaxima[3] + 1)) + 1;
		// Rest time.
		if(Math.random() < mutationRate)
			gene[i+4] = Math.floor(Math.random() *  (Skeleton.GeneMaxima[4] + 1)) + 1;
		// Stuck time.
		if(Math.random() < mutationRate)
			gene[i+5] = Math.floor(Math.random() *  (Skeleton.GeneMaxima[4] + 1)) + 1;
		// Active at start.
		if(Math.random() < mutationRate)
			gene[i+6] = Math.random() < .5;
	}
}

Skeleton.prototype = {
    addBone:function(world, startPos, endPos, parent){
        if(this.boneCount > Skeleton.BoneLimit ||(this.boneCount > 0 && parent == -1)){
            return;
        }
        
        // Determine bone angle
        var dX = endPos.x - startPos.x
        var dY = startPos.y - endPos.y;
        var angle = Math.abs(Math.atan((dY)/(dX)));

        if(dX < 0){
            if(dY < 0)
                angle = Math.PI + angle;
            else
                angle = Math.PI - angle;
        }   
        else if(dY < 0){
            angle = 2 * Math.PI - angle;
        }
        var dist = Math.sqrt(dX*dX + dY*dY);
        
        if(dist < 10){
            return;
        }

        this.createBone(world,{"angle":-angle,"pos":{"x":startPos.x + dX/2, "y":startPos.y - dY/2},"length":dist/2});
                          
        // create a joint when with parent.
        if(parent != -1 && parent < this.boneCount){
			this.createJoint(world,{"bone":this.bones.length - 1,
									"pBone":parent, 
									"joint":this.bones[this.bones.length - 1].joints.length,
									"anchor":{"x":startPos.x,"y":startPos.y}});
        }      
        
        // Create the "head"
        if(this.boneCount == 1){
            this.createHead(world,startPos.x, startPos.y);
        }
    },
	
	createBone:function(world, transform){
	    this.boneCount++;

		// Shape Definition
        var boneSd = new b2BoxDef();
        boneSd.extents.Set(transform.length, Skeleton.Width);
        boneSd.density = Skeleton.Density;
		boneSd.categoryBits = Skeleton.BoneMask;
		boneSd.maskBits     = Skeleton.MaskBase - Skeleton.BoneMask;
        boneSd.friction = 1;        
        
        // Body definition
        var boneBd = new b2BodyDef();
        boneBd.AddShape(boneSd);
        boneBd.rotation = transform.angle; // Box2d requires this negative to play nice
        boneBd.position.Set(transform.pos.x,transform.pos.y);        
        boneBd.userData = { "bone":this.bones.length};
        boneBd.allowSleep = false;
		
		this.bones.push({"body":world.CreateBody(boneBd), 
		  "def": boneBd,
		  "transform":transform,
		  "joints":[],
		  "jDefs":[]});
	},
	
	createJoint:function(world, bodies){
		var jointDef = new b2RevoluteJointDef();
            
		jointDef.body1 = this.bones[bodies.pBone].body;
		jointDef.body2 = this.bones[bodies.bone].body;

		jointDef.anchorPoint.x = bodies.anchor.x;
		jointDef.anchorPoint.y = bodies.anchor.y;    
	   
		jointDef.enableMotor = true;
		jointDef.enableLimit = true;
		
		jointDef.lowerAngle = -1;
		jointDef.upperAngle = 1;
		jointDef.motorTorque = Skeleton.MaxTorque;

		var joint = world.CreateJoint(jointDef);
		
		var jointMotor = {"speed":0,"active":0,"rest":0,"stuckTime":0, "isActive":false,"aV":0,"rV":0,"flip":1, "limit":{"up":0,"low":0}, "stuckCount":0,"prevAngle":0};
		
		this.bones[bodies.bone].joints.push(joint); 
		this.bones[bodies.bone].jDefs.push({"def":jointDef, "parent":bodies.pBone,"motor":jointMotor});

		
		// This is used to streamline joint operations in the update loop, keeping a shallow copy of the bone data.
		this.joints.push({"def":jointDef, 
			"motor":jointMotor, 
			"body":bodies});
			
		Skeleton.AppendJoint();
	},
	
    createHead:function(world,x,y){
		var ballSd = new b2CircleDef();
		ballSd.density = Skeleton.Density;
		ballSd.radius = Skeleton.HeadRadius;
		ballSd.restitution = 0;
		ballSd.friction = 0;
		var ballBd = new b2BodyDef();
		ballBd.AddShape(ballSd);
		ballBd.position.Set(x, y);
		ballBd.userData = { "head":this.headCallBack};
		ballBd.allowSleep = false;

		this.head = {"body":world.CreateBody(ballBd),
					"def": ballBd,
					"pos":{"x":x, "y":y},
					"joint":{},
					"jDef":{}};
		
		// Connect to the "torso"
		var jointDef = new b2RevoluteJointDef();
		
		// Always will be zero.
		jointDef.body2 = this.bones[0].body;
		jointDef.body1 = this.head.body;

		jointDef.anchorPoint = ballBd.position;
		jointDef.collideConnected = false;
		jointDef.enableMotor = false;
		jointDef.enableLimit = false;

		this.head.joint = world.CreateJoint(jointDef);
		
		this.head.jDef = jointDef;
	},
	
    applyGene:function(gene){
        var loc = 0;

        for(var joint = 0; joint < this.joints.length; joint++){
            loc = joint * Skeleton.GeneMaxima.length;
			
			this.joints[joint].def.upperAngle   = gene.gene[loc];
            this.joints[joint].def.lowerAngle   = gene.gene[loc+1];

			this.joints[joint].motor.speed      = gene.gene[loc+2];
			this.joints[joint].motor.active     = gene.gene[loc+3];
			this.joints[joint].motor.rest       = gene.gene[loc+4];
			this.joints[joint].motor.stuckTime  = gene.gene[loc+4];
			this.joints[joint].motor.isActive   = gene.gene[loc+6];

			this.joints[joint].motor.aV         = 0;
			this.joints[joint].motor.rV         = 0;			
			this.joints[joint].motor.flip       = 1;
			this.joints[joint].motor.limit.up   = gene.gene[loc]/10;
			this.joints[joint].motor.limit.low  = gene.gene[loc+1]/10;	
			this.joints[joint].motor.stuckCount = 0;	
			this.joints[joint].motor.prevAngle  = 0;
        }
    },
    
    reset:function(world){
        this.kill(world);
        
        for(var bone=0;bone<this.bones.length;bone++){
            this.bones[bone].body = world.CreateBody(this.bones[bone].def);

            for(var joint = 0; joint < this.bones[bone].joints.length;joint++){
                this.bones[bone].jDefs[joint].def.body1 = this.bones[this.bones[bone].jDefs[joint].parent].body;
                this.bones[bone].jDefs[joint].def.body2 = this.bones[bone].body;
                this.bones[bone].joints[joint] = world.CreateJoint(this.bones[bone].jDefs[joint].def);
            }
        }

        // Remake the head.
        this.head.body = world.CreateBody(this.head.def);
        this.head.jDef.body1 = this.head.body;
        this.head.jDef.body2 = this.bones[0].body;
        this.head.joint = world.CreateJoint(this.head.jDef);
    },  
    
    kill:function(world){
        world.DestroyBody(this.head.body);
        world.DestroyJoint(this.head.joint);
        for(var bone=0;bone<this.bones.length;bone++){
            world.DestroyBody(this.bones[bone].body);
            this.bones[bone].body = null;
            
            for(var joint = 0; joint < this.bones[bone].joints.length;joint++){
                world.DestroyJoint(this.bones[bone].joints[joint]);
                this.bones[bone].joints[joint] = null;
            }            
        }
        
        // This ensures that all the bodies are gone after reset.
        world.Step(1.0/60.0,1);
        world.GetBodyList();
    },
    
    destroy:function(world){
        if(this.boneCount == 0)
            return;
            
        this.kill(world);
        // reset the member variables.
        Skeleton.Genome = [];
        this.bones      = [];
        this.joints     = []; 
        this.head       = {};
        this.boneCount  = 0;
    }, 
	
	exportSkeleton:function(exportObj){
		exportObj.skeleton = {"bones":[],"joints":[],"head":this.head.pos};
		//Append the bones to the export object
		for(var bone = 0; bone < this.bones.length; bone++){
			exportObj.skeleton.bones.push(this.bones[bone].transform);
		}
		
		for(var joint = 0; joint < this.joints.length; joint++){
			exportObj.skeleton.joints.push(this.joints[joint].body);
		}	
	},
	
	importSkeleton:function(world, skeleton){	
		if(!skeleton.head)
			return false;
			
		this.destroy(world);
			
		for(var bone = 0; bone < skeleton.bones.length; bone++){
			this.createBone(world, skeleton.bones[bone]);
		}	
		
		for(var joint = 0; joint < skeleton.joints.length; joint++){
			this.createJoint(world, skeleton.joints[joint]);
		}
		
		this.createHead(world,skeleton.head.x, skeleton.head.y);					
		
		return true;
	}	
}


